import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';


declare var $:any;

@Component({
    selector: 'dashboard-cmp',
    moduleId: module.id,
    styleUrls: ['./dashboard.component.css'],
    templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit{

  public start_time : string = '';
  public end_time : string = '';
  public time_slot:string='';
  httpOptions : any;
  data:string;
  url : string = 'www.ayushyaaclinic.com/api/public/';

  slottiming:any;
  AllCompletedToken:any;
  TodayTokenpending:any;
  Todaycompletedtoken:any;
  status:any;
  currentStatus:any;
  ONOFFslot: boolean = false;
  constructor(public http: Http, public router: Router) { }

  ngOnInit(){
    let win = (window as any);
    if(!sessionStorage.getItem('reload')) {
      sessionStorage.setItem('reload','reload')
      win.location.reload();
    }
    this.Completed_Token();
    this.GetTodayToken(); 
    this.StatusONOFFdetail();    
  }

  slot_time(){
    this.data = "start_time="+this.start_time+'&end_time='+this.end_time+'&slot='+this.time_slot;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.http.post('http://'+this.url+'Create_slot', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
      if(!data.error){
        this.showNotification(data.message, 2);
        this.getSlotTime();  
      }else{ 
        this.showNotification(data.message, 4);     
      }
    });
  }

  getSlotTime(){
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.http.get('http://'+this.url+'gettodaySlotDetails',this.httpOptions).map(res => res.json()).subscribe(data => {
      this.slottiming=data.data;
      //console.log(this.slottiming);
    });
  }

  showD(_d){
    if(_d == '1'){
      this.start_time =  this.slottiming[0].start_time;
      this.end_time = this.slottiming[0].end_time;
    }
    else{
      this.start_time =  this.slottiming[1].start_time;
      this.end_time = this.slottiming[1].end_time;
    }
  }

  Completed_Token(){
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.http.get('http://'+this.url+'getallcompletedTokenHistory',this.httpOptions).map(res => res.json()).subscribe(data => {
      this.AllCompletedToken=data.data;
    });
  }
    
  GetTodayToken(){
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.http.get('http://'+this.url+'GetAllToken',this.httpOptions).map(res => res.json()).subscribe(data => {
      this.TodayTokenpending=data.pending;
      this.Todaycompletedtoken=data.completed;
      this.getSlotTime();
    });
  }

  cacelledSlot(slot,send_message){
    this.data = "stopSlot="+slot+"&message="+send_message;   
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.http.post('http://'+this.url+'stopSlot', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
      if(!data.error){
        this.showNotification(data.message, 2);
        this.getSlotTime();
      }else{            
        this.showNotification(data.message, 4);
      }
    });

  }
  StatusONOFFdetail(){
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.http.get('http://'+this.url+'getslotissualstatus',this.httpOptions).map(res => res.json()).subscribe(data => {
      this.ONOFFslot= (data.data[0].status == '1')? true:false;
      console.log(this.ONOFFslot);
    });
  }

  OnnOffSlot(val){
    this.data = "status="+val;   
    this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded'
        })
    };
    this.http.post('http://'+this.url+'setSlotOff', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
        if(!data.error){
          this.showNotification(data.message, 2);
          this.StatusONOFFdetail();
        }
        else{
          this.showNotification(data.message, 4);
        }
    });
  }

  showNotification(message, color){
    var type = ['','info','success','warning','danger'];

    //var color = Math.floor((Math.random() * 4) + 1);

  $.notify({
      icon: "ti-info-alt",
      message: message
    },{
        type: type[color],
        timer: 3000,
        placement: {
            from: 'top',
            align: 'center'
        }
    });
}
       
}
