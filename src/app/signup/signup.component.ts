import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
declare var $:any;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

   name : string = '';
   phone : string = '';
   email : string = '';
   dob : string = '';
   password : string = '';
   conf_pwd : string = '';

   otpFlag:boolean = true;
   verifyOtpFlag :boolean = false;
   changePassFlag:boolean = false;
   otp : string = '';

  httpOptions : any;
  data:string;
  url : string = 'www.ayushyaaclinic.com/api/public/';
  loading: boolean = false;

  constructor(public http: Http, public router: Router) { }
  ngOnInit() { 
    if(localStorage.loginData){
      this.router.navigate(['/current-token']);
    }
  }
  doSignup(){
    if(this.name == '' || this.phone == '' || this.email == '' || this.dob == '' || this.password == '' || this.conf_pwd == ''){
      this.showNotification('Please fill all the details.', 4);
      return;
    }
    if(this.phone.length != 10 ){
      this.showNotification('Please enter 10 digit phone number', 4);
      return;
    }
    if(this.password.length < 3 ){
      this.showNotification('Please enter minimum 4 characters', 4);
      return;
    }
    if(this.password != this.conf_pwd){
      this.showNotification('Password Missmatch.', 4);
      return;
    }
    if(this.password == this.conf_pwd){
      this.data = 'name='+this.name+'&phone='+this.phone+'&email_id='+this.email+'&dob='+this.dob+'&password='+this.password;
      this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded'
        })
      };
      this.loading = true;
      this.http.post('http://'+this.url+'User_Registration', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
        this.loading = false;
        if(!data.error){
          this.showNotification(data.message, 2);
          localStorage.setItem('loginData', JSON.stringify(data.data));
          this.router.navigate(['/current-token']);
          //window.location.reload();
        }else{
          this.showNotification(data.message, 4);
        }
      });
    }else{
      this.showNotification('password mismatch', 4);
    }

  }

  openLogin(){
    this.router.navigate(['/login']);
    window.location.reload();
  }

  openForgotPass(){
    this.router.navigate(['/forgotpwd']);
    window.location.reload();
  }

  getOtp(){
    if(this.phone.length != 10){
      this.showNotification('Please enter 10 digit phone number.', 4);
      return;
    }
    this.data = 'phone='+this.phone+'&type=0';
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.loading = true;
    this.http.post('http://'+this.url+'ForgotPassword', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
      this.loading = false;
      if(!data.error){
        this.otpFlag = false;
        this.verifyOtpFlag = true;
        this.changePassFlag = false;
      }else{
        this.showNotification(data.message, 4);
      }
    });

}

verifyOtp(){
    this.data = 'phone='+this.phone+'&otp='+this.otp;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.loading = true;
    this.http.post('http://'+this.url+'VerifyOTPForgot', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
      this.loading = false;  
    if(!data.error){
        this.otpFlag = false;
        this.verifyOtpFlag = false;
        this.changePassFlag = true;
      }else{
        this.showNotification(data.message, 4);
      }
    });

}

  showNotification(message, color){
    var type = ['','info','success','warning','danger'];

    //var color = Math.floor((Math.random() * 4) + 1);

  $.notify({
      icon: "ti-info-alt",
      message: message
    },{
        type: type[color],
        timer: 3000,
        placement: {
            from: 'top',
            align: 'center'
        }
    });
}

}
