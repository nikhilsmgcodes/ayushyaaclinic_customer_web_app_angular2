import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';

@Component({
  selector: 'app-token',
  templateUrl: './token.component.html',
  styleUrls: ['./token.component.css']
})
export class TokenComponent implements OnInit {
  public patient_name : string = '';
  public gender : string = '';
  public phone_number:string='';
  public dateofbirth:string;
  public address:string=''; 
  public convinient_time:string='';
  httpOptions : any;
  data:string;
  url : string = 'www.ayushyaaclinic.com/api/public/';
  filtertoken : string = '';
  filtertokennumber : string;
  AllCompletedToken:any;
  TodayTokenpending:any;
  Todaycompletedtoken:any;
  CurrenntToken:any;
  start_time:any;
  
  constructor(public http: Http, public router: Router) { }
  ngOnInit() {
    this.Completed_Token();
    this.GetTodayToken();
  }

  Completed_Token(){
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.http.get('http://'+this.url+'getallcompletedTokenHistory',this.httpOptions).map(res => res.json()).subscribe(data => {
      this.AllCompletedToken=data;
      console.log(this.AllCompletedToken);
    });
  }

  GetTodayToken(){
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.http.get('http://'+this.url+'GetAllToken',this.httpOptions).map(res => res.json()).subscribe(data => {
      this.TodayTokenpending=data.pending;
      this.Todaycompletedtoken=data.completed;
      this.CurrenntToken=data.current;
    });
  }

  ONStartTime(Token_number){
    this.data = "Token_number="+Token_number;   
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.http.post('http://'+this.url+'UpdateStarttime', this.data, this.httpOptions).map(res => res.json()).subscribe(data =>{
      if(!data.error){
        alert(data.message);
        this.GetTodayToken();
        this.Completed_Token();
      }else{
        alert(data.message);
      }        
    });
  }

  ONEndTime(Token_number){
    this.data = "Token_number="+Token_number;   
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.http.post('http://'+this.url+'Endtoken', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
      if(!data.error){
        alert(data.message);
        this.GetTodayToken();
        this.Completed_Token();
      }else{
        alert(data.message);
      }       
    });
  }

  next_token(Token_number){
    this.data = "currentToken="+Token_number;   
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.http.post('http://'+this.url+'nextToken', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
      if(!data.error){
        alert(data.message);
        this.GetTodayToken();
        this.Completed_Token();
      }else{
        alert(data.message);
      }       
    });
  }

}