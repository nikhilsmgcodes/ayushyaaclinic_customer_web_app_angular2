import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
// import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent{
  public hideSideBar : boolean = false;
  constructor(public router: Router, public location: Location){
    router.events.subscribe((val) => {

      if(location.path()=='/login' || location.path()=='/signup' || location.path()=='/forgotpwd'){
        this.hideSideBar = true;

      }else{

        this.hideSideBar = false;
      }
      // if(location.path() != ''){
      //   this.route = location.path();
      // } else {
      //   this.route = 'Home'
      // }
    });
  }
  ngOnInit(){
    // console.log(this.router.url);
  }
}
