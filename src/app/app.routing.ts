import { Routes } from '@angular/router';

// import { DashboardComponent }   from './dashboard/dashboard.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { ForgotpwdComponent } from './forgotpwd/forgotpwd.component';
import { TakeTokenComponent } from './take-token/take-token.component';
import { CurrentTokenComponent } from './current-token/current-token.component';
import { ProfileComponent } from './profile/profile.component';
import { UserComponent }   from './user/user.component';
import { TableComponent }   from './table/table.component';
import { TypographyComponent }   from './typography/typography.component';
import { IconsComponent }   from './icons/icons.component';
import { MapsComponent }   from './maps/maps.component';
import { NotificationsComponent }   from './notifications/notifications.component';
import { UpgradeComponent }   from './upgrade/upgrade.component';
import { TokenComponent } from './token/token.component';



export const AppRoutes: Routes = [
    {
        path: 'signup',
        component: SignupComponent
    },
    {
        path: 'forgotpwd',
        component: ForgotpwdComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'take-token',
        component: TakeTokenComponent
    },
    {
        path: 'current-token',
        component: CurrentTokenComponent
    },
    {
        path: 'profile',
        component: ProfileComponent
    },
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    },
]
