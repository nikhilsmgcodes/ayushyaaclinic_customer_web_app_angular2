import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';

declare var $:any;
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  httpOptions : any;
  data:string;
  url : string = 'www.ayushyaaclinic.com/api/public/';
  allTodaysToken : any;
  errorMessage : string;
  name : string = '';
  email : string = '';
  dob : string = '';
  phone_no: string;
  old_pwd : string = '';
  new_pwd : string = '';
  conf_pwd : string= '';
  loading: boolean = false;

  constructor(public http: Http, public router: Router) {
    this.allTodaysToken = [];
  }

  ngOnInit() {
    if(!localStorage.loginData){
      this.router.navigate(['/login']);
      window.location.reload();
    }
    this.name = JSON.parse(localStorage.getItem('loginData')).name;
    this.email = JSON.parse(localStorage.getItem('loginData')).email_id;
    this.dob = JSON.parse(localStorage.getItem('loginData')).dob;
    this.phone_no = JSON.parse(localStorage.getItem('loginData')).phone;
    this.getMyAllTokens();
  }

  myAcoountForm(){
    if(this.name == '' || this.dob == '' || this.email == ''){
      this.showNotification('Please fill all the details', 4);
      return;
    }
    this.data = "user_id="+JSON.parse(localStorage.getItem('loginData')).user_id+'&name='+this.name+'&email_id='+this.email+'&dob='+this.dob+'&phone='+this.phone_no;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.loading = true;
    this.http.post('http://'+this.url+'Update_Profile', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
      this.loading = false; 
    if(!data.error){
        var tData = {
          user_id : JSON.parse(localStorage.getItem('loginData')).user_id,
          name : this.name,
          phone : this.phone_no,
          email_id : this.email,
          dob : this.dob,
          loginType : JSON.parse(localStorage.getItem('loginData')).loginType,
          socialUserId : JSON.parse(localStorage.getItem('loginData')).socialUserId
        };
        localStorage.setItem('loginData', JSON.stringify(tData));
        this.showNotification(data.message, 2);
      }else{
        this.showNotification(data.message, 4);
      }
    });
  }

  getMyAllTokens(){
    this.data = "user_id="+JSON.parse(localStorage.getItem('loginData')).user_id;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.loading = true;
    this.http.post('http://'+this.url+'getPastDetailsByPhone', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
      this.loading = false;  
    if(!data.error){
        this.allTodaysToken = data.data;
      }else{
        this.allTodaysToken = [];
        this.errorMessage = data.data;
      }
    });
  }

  changePwdForm(){
    if(this.new_pwd.length <= 4){
      this.showNotification('Please enter minimum 4 characters.', 4);
      return;
    }
    if(this.new_pwd != this.conf_pwd){
      this.showNotification('Password Missmatch.', 4);
      return;
    }
    if(this.new_pwd == this.conf_pwd){
      this.data = "user_id="+JSON.parse(localStorage.getItem('loginData')).user_id+'&old_password='+this.old_pwd+'&new_password='+this.new_pwd;
      this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded'
        })
      };
      this.loading = true;
      this.http.post('http://'+this.url+'Update_Password', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
        this.loading = false;  
      if(!data.error){
          this.new_pwd = '';
          this.conf_pwd = '';
          this.old_pwd = '';
          setTimeout(function(){
            this.showNotification(data.message, 2);
          },0);

        }else{
          this.showNotification(data.message, 4);
        }
      });

    }
    else{
      this.showNotification('password mismatch', 4);
    }

  }

  showNotification(message, color){
    var type = ['','info','success','warning','danger'];

    //var color = Math.floor((Math.random() * 4) + 1);

  $.notify({
      icon: "ti-info-alt",
      message: message
    },{
        type: type[color],
        timer: 3000,
        placement: {
            from: 'top',
            align: 'center'
        }
    });
}

}
