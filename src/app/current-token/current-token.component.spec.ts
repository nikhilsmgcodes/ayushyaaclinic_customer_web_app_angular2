import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentTokenComponent } from './current-token.component';

describe('CurrentTokenComponent', () => {
  let component: CurrentTokenComponent;
  let fixture: ComponentFixture<CurrentTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
