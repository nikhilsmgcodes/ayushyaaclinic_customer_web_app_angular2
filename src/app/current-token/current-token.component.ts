import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';


@Component({
  selector: 'app-current-token',
  templateUrl: './current-token.component.html',
  styleUrls: ['./current-token.component.css']
})
export class CurrentTokenComponent implements OnInit {
  httpOptions : any;
  data:string;
  url : string = 'www.ayushyaaclinic.com/api/public/';
  currentToken : string = '---';
  allTodaysToken : any;
  errorMessage : string;
  upcommingToken1: string = '---';
  upcommingToken2: string = '---';
  loading: boolean = false;
  constructor(public http: Http, public router: Router) {
    this.allTodaysToken = [];
  }

  ngOnInit() {
    if(!localStorage.loginData){
      this.router.navigate(['/login']);
      window.location.reload();
    }
    // if(!localStorage.refresh){
    //   localStorage.setItem('refresh','refresh');
    //   window.location.reload();
    // }
    this.allTodaysToken = [];
    this.getCurrentToken();
    this.getMyAllTokens();
  }
  getCurrentToken(){
    this.loading = true;
      this.http.get('http://'+this.url+'GetAllToken').map(res => res.json()).subscribe(data => {
        this.loading = false;
      if(data.current.length>0){
          this.currentToken = data.current[0].token_number;
        }else{
          this.currentToken = '---';
        }
        if(data.pending.length>0){
          this.upcommingToken1 = data.pending[0].token_number;
          if(data.pending.length>1){
            this.upcommingToken2 = data.pending[1].token_number;
          }
          else{
            this.upcommingToken2 = '---';
          }
        }
        else{
          this.upcommingToken1 = '---';
        }
      });

  }


  getMyAllTokens(){
    this.data = "user_id="+JSON.parse(localStorage.getItem('loginData')).user_id;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.loading = true;
    this.http.post('http://'+this.url+'getTokenDetailsBydate', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
      this.loading = false;  
    if(!data.error){
        this.allTodaysToken = data.data;
      }else{
        this.allTodaysToken = [];
        this.errorMessage = data.data;
      }
    });
  }



}
