import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';

declare var $:any;
@Component({
  selector: 'app-forgotpwd',
  templateUrl: './forgotpwd.component.html',
  styleUrls: ['./forgotpwd.component.css']
})
export class ForgotpwdComponent implements OnInit {

  otpFlag:boolean = true;
  verifyOtpFlag :boolean = false;
  changePassFlag:boolean = false;

  phone : string = '';
  otp : string = '';
  password : string = '';
  conf_password: string = '';
  httpOptions : any;
  data:string;
  url : string = 'www.ayushyaaclinic.com/api/public/';
  loading: boolean = false;
  constructor(public http: Http, public router: Router) { }
  ngOnInit() {
    if(localStorage.loginData){
      this.router.navigate(['/current-token']);
    }
  }
  getOtp(){
      if(this.phone.length != 10){
        this.showNotification('Please enter 10 digit phone number.', 4);
        return;
      }
      this.data = 'phone='+this.phone+'&type=1';
      this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded'
        })
      };
      this.loading = true;
      this.http.post('http://'+this.url+'ForgotPassword', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
        this.loading = false;
        if(!data.error){
          this.otpFlag = false;
          this.verifyOtpFlag = true;
          this.changePassFlag = false;
        }else{
          this.showNotification(data.message, 4);
        }
      });

  }

  verifyOtp(){
      this.data = 'phone='+this.phone+'&otp='+this.otp;
      this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded'
        })
      };
      this.loading = true;
      this.http.post('http://'+this.url+'VerifyOTPForgot', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
        this.loading = false;  
      if(!data.error){
          this.otpFlag = false;
          this.verifyOtpFlag = false;
          this.changePassFlag = true;
        }else{
          this.showNotification(data.message, 4);
        }
      });

  }

  updatePwd(){
    if(this.password.length < 3){
      this.showNotification('Please enter minimum 4 characters.', 4);
      return;
    }
    if(this.conf_password != this.password){
      this.showNotification('Password MissMatch.', 4);
      return;
    }
      this.data = 'phone='+this.phone+'&password='+this.password+'&con_password='+this.conf_password;
      this.httpOptions = {
        headers: new Headers({
          'Content-Type':  'application/x-www-form-urlencoded'
        })
      };
      this.loading = true
      this.http.post('http://'+this.url+'ChangePassword', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
        this.loading = false;  
      if(!data.error){
        this.showNotification(data.message, 2);
          //localStorage.setItem('loginData', JSON.stringify(data.data));
          this.router.navigate(['/login']);
          window.location.reload();
        }else{
          this.showNotification(data.message, 4);
        }
      });

  }

  openLogin(){
    this.router.navigate(['/login']);
    window.location.reload();
  }

  openSignup(){
    this.router.navigate(['/signup']);
    window.location.reload();
  }

  showNotification(message, color){
    var type = ['','info','success','warning','danger'];

    //var color = Math.floor((Math.random() * 4) + 1);

  $.notify({
      icon: "ti-info-alt",
      message: message
    },{
        type: type[color],
        timer: 3000,
        placement: {
            from: 'top',
            align: 'center'
        }
    });
}
}
