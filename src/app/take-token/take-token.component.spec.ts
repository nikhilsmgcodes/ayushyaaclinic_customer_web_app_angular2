import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TakeTokenComponent } from './take-token.component';

describe('TakeTokenComponent', () => {
  let component: TakeTokenComponent;
  let fixture: ComponentFixture<TakeTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TakeTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TakeTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
