import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';

declare var $:any;
@Component({
  selector: 'app-take-token',
  templateUrl: './take-token.component.html',
  styleUrls: ['./take-token.component.css']
})
export class TakeTokenComponent implements OnInit {
  no_of_patient = 1;
  name = '';
  phone = '';
  dob = '';
  email= '';
  select_numbertoken;
  httpOptions : any;
  data:string;
  url : string = 'www.ayushyaaclinic.com/api/public/';
  loading: boolean = false;
  constructor(public http: Http, public router: Router) { 
    this.select_numbertoken = [
      {
        label: "1",
        value: 1
      }, {
        id: 2,
        label: "2",
        value: 2
      }, {       
        label: "3",
        value: 3
      },
      {       
        label: "4",
        value: 4
      },
      {       
        label: "5",
        value: 5
      },
      {       
        label: "6",
        value: 6
      },
      {       
        label: "7",
        value: 7
      },
      {       
        label: "8",
        value: 8
      },
      {       
        label: "9",
        value: 9
      },      
       {
        label: "10",
        value: 10
      }
    ];
  }

  ngOnInit() {
    if(!localStorage.loginData){
      this.router.navigate(['/login']);
      window.location.reload();
    }
  }
  submitForm(tokenFor){
    if(tokenFor == 'other'){
      if(this.name == '' || this.phone == '' || this.dob == '' || this.email == ''){
        this.showNotification('Please fill all the details', 4);
        return;
      }
      if(this.phone.length != 10){
        this.showNotification('Please enter 10 digit phone number', 4);
        return;
      }
    }
    this.data = "user_id="+JSON.parse(localStorage.getItem('loginData')).user_id+'&phone='+this.phone+'&patient_name='+this.name+'&NoOfPatient='+this.no_of_patient+'&dob='+this.dob+'&email='+this.email+'&TokenFor='+tokenFor;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.loading = true;
    this.http.post('http://'+this.url+'Token_Generate', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
      this.loading = false;
      if(!data.error){
        this.name = '';
        this.email = '';
        this.no_of_patient = 1;
        this.phone = '';
        this.dob = '';
        this.router.navigate(['/current-token']);
      }else{
        this.showNotification(data.message, 4);
      }
    });
  }

  showNotification(message, color){
    var type = ['','info','success','warning','danger'];

    //var color = Math.floor((Math.random() * 4) + 1);

  $.notify({
      icon: "ti-info-alt",
      message: message
    },{
        type: type[color],
        timer: 3000,
        placement: {
            from: 'top',
            align: 'center'
        }
    });
}

}
