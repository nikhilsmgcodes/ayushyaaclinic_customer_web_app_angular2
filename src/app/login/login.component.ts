import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';

declare var $:any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public phone : string = '';
  public password : string = '';
  httpOptions : any;
  data:string;
  url : string = 'www.ayushyaaclinic.com/api/public/';
  loading: boolean = false;
  constructor(public http: Http, public router: Router) { }
  ngOnInit() {
    if(localStorage.loginData){
      this.router.navigate(['/current-token']);
    }
  }
  doLogin(){
    if(this.phone == '' || this.password == ''){
      this.showNotification('Please fill the details', 4);
      return;
    }
    if(this.phone.length != 10){
      this.showNotification('Please enter 10 digit phone number', 4);
      return;
    }
    this.data = "phone="+this.phone+'&password='+this.password;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.loading = true;
    this.http.post('http://'+this.url+'User_login', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
      this.loading = false;  
    if(!data.error){
      this.showNotification(data.message, 2);
        localStorage.setItem('loginData', JSON.stringify(data.data));
        this.router.navigate(['/current-token']);
      }else{
        this.showNotification(data.message, 4);
      }
    });
  }
  
  openSignup(){
    this.router.navigate(['/signup']);
    window.location.reload();
  }

  openForgotPass(){
    this.router.navigate(['/forgotpwd']);
    window.location.reload();
  }

  showNotification(message, color){
    var type = ['','info','success','warning','danger'];

    //var color = Math.floor((Math.random() * 4) + 1);

  $.notify({
      icon: "ti-info-alt",
      message: message
    },{
        type: type[color],
        timer: 3000,
        placement: {
            from: 'top',
            align: 'center'
        }
    });
}

}
